"""
These are the loot tables for fights, and possibly other things. All of
the loot functionality is included here. Each loot table has a loot_type, a
variable that determines exactly how each loot table functions. The
loot_types are as follows:

None: normal, num is the number of times it's rolled
'g': guaranteed, at least num items from this will drop
'a': always, all items in this will drop, regardless of num

items is a list with all of the possible items.

cash is a two-value tuple that contains the amount minimum and maximum
amount of money that can be dropped by an actor. Remember that actors
with multiple loot tables will probably drop more cash than just one
loot table's worth, so plan around that when designing boss loot.
You can set the first variable to a negative value. If the RNG picks a
negative number for cash, the player instead gets 0 cash.

same is a bool that, if true, will allow two items of the same kind to
drop.

When doing loot, make each actor have a list of all of its loot
tables, then make a for loop going through all of its loot tables. This
is so that you can have enemies that always drop one item, always drop
one of several items, then drop a bunch of other random shit.
"""

from random import shuffle, random, randint
import items
import player


class Loot:
    def __init__(self, cash, item_list, loot_type, num, same):
        self.cash = cash
        self.item_list = item_list
        self.loot_type = loot_type
        self.num = num
        self.same = same

    def begin_looting(self, loot_cont):
        # CALL THIS FUNCTION FIRST WHEN LOOTING
        # this picks the loot loot_type, then runs the appropriate function
        # for dishing out loot
        # loot_cont is a list used for counting all of the loot after
        # it's been handed out by the loot tables

        # add cash first, since that always happens
        cash_given = randint(self.cash[0], self.cash[1])

        if cash_given < 0:
            cash_given = 0
        player.you.cash += cash_given

        table_loot_types = {
            None: self.numbered_loot,
            'g': self.numbered_loot,
            'a': self.always_loot
        }

        return table_loot_types[self.loot_type](loot_cont)

    def numbered_loot(self, loot_cont):
        # this handles both normal and always loot loot_types

        # first, remove bias from list order
        shuffle(self.item_list)

        # i is used to iterationate through the items_list
        # n is used to count the proper number of times we've either
        # rolled or dropped items, depending on self.loot_type
        i = 0
        n = 0
        while n < self.num:
            # if a normal loot table
            if self.loot_type is None: n += 1
            loot_roll = 100 * random()
            if self.item_list[i].rarity >= loot_roll:
                # print(self.item_list[i].name)  # DBGGR
                loot_cont.append(self.item_list[i])
                # if an always loot table
                if self.loot_type == 'g': n += 1
            i += 1
            i = self.g_iteration_reset(self.item_list, i)

        return loot_cont

    def always_loot(self, loot_cont):
        for i in range(0, len(self.item_list)):
            print(self.item_list[i].name)
            loot_cont.append(self.item_list[i])
            return loot_cont

    # noinspection PyMethodMayBeStatic
    def g_iteration_reset(self, i_list, iteration):
        # resets i to 0 if iteration is greater than i_list length
        # ORIGINALLY THIS WAS LIMITED TO GUARANTEED LOOT ONLY, IF
        # SOMETHING BREAKS LOOK INTO THAT
        if iteration >= len(i_list):
            iteration = 0
        return iteration


###############################################################################
# LOOT LISTS                                                                  #
###############################################################################

test_items = [items.weed, items.coke]
dindu_items = [items.weed, items.coke]
whore_items = []

###############################################################################
# LOOT TABLES                                                                 #
###############################################################################

test_norm = Loot(
    (0, 100),  # cash
    test_items,  # item_list
    None,  # loot_type
    5,  # num
    0  # same
)

dindu_list = Loot(
    (-10, 50),  # cash
    dindu_items,  # item_list
    None,  # loot_type
    3,  # num
    0  # same
)

whore_list = Loot(
    (-200, 500),  # cash
    whore_items,  # item_list
    None,  # loot_type
    3,  # num
    0  # same
)

# watch_hawker
# drug_dealer
# policeman
# rabid_dog
# wealthy_guy
# mugger
# hobo