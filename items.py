"""
This is where all of the items in the game are cotained. Pretty much no
item in the game should just be an Item, all items should be subclasses
such as Equipment or Consumable.

todo:
- make Consumable a child of Usable (a new class)
- add 'special' to all items

maybe:
- add a specific function for adding items to the player's inventory
    - probably add this to player.py
"""

import player


class Item:
    def __init__(self, name, nick, price, rarity):
        self.name = name
        self.nick = nick
        self.price = price
        self.rarity = rarity  # chance to drop if selected by loot roll


class Equipment(Item):
    def __init__(self, name, nick, price, rarity, slot, bonus_stat, stat_increase_val):
        super().__init__(name, nick, price, rarity)
        self.slot = slot
        self.bonus_stat = bonus_stat  # the stat to increase
        self.stat_increase_val = stat_increase_val  # the value to increase or decrease by


class Usable(Item):
    def __init__(self, name, nick, price, rarity, use_func):
        super().__init__(name, nick, price, rarity)
        self.use_func = use_func  # this is the function that causes the effect


class Consumable(Usable):
    def __init__(self, name, nick, price, rarity, use_func):
        super().__init__(name, nick, price, rarity, use_func)


###############################################################################
# USE FUNCTIONS                                                               #
###############################################################################

def verb_use(input_list):
    # this is for using items in your inventory
    # first, make sure you have that item in your inventory
    # then use it
    # if the item is a consumable, remove it from player's inventory

    if len(input_list) == 1:
        incomplete_phrase(input_list)
        return

    item_to_use = find_item_in_inv(input_list[1])

    if item_to_use is None:
        print("You can't use that item!")
        return

    item_to_use.use_func()
    if type(item_to_use) == Consumable:  # if item_to_use is a Consumable:
        player.you.inventory.remove(item_to_use)  # remove from player's inventory


def advil_use():
    print("NYI: Add 10 temporary hitpoints")

def coke_use():
    print("NYI: Add one stack of Jittery, or remove one of Drowzy.")


def weed_use():
    print("NYI: Add one stack of Drowzy, or remove one of Jittery.")


###############################################################################
# MISC FUNCTIONS                                                              #
###############################################################################

def find_item_in_inv(item_to_find):
    # checks to see if item_to_find exists in the player's inventory
    for i in range(0, len(player.you.inventory)):
        if player.you.inventory[i].nick == item_to_find:
            return player.you.inventory[i]


def incomplete_phrase(input_list):
    # this function is stuck in verb functions that ALWAYS require a
    # a subject and an object in their phrasing

    # the following code must be included in any verb that requires
    # more than one word:

    """
    if len(input_list) == 1:
        incomplete_phrase(input_list)
        return
    """

    print("%s what?" % input_list[0])


###############################################################################
# EQUIPMENT                                                                   #
###############################################################################

helm = Equipment('Helmet', 'helm', 100, 100, player.you.head, player.you.stat_block.hp, 10)
cap = Equipment('Cap', 'cap', 5, 100, player.you.head, None, None)
# todo make these
# beanie

# pocket_knife
# switchblade
# lead_pipe
# baseball_bat

# xxl_bra
# soccer_jersey

# trash_can_lid

# cup

###############################################################################
# GUNS                                                                        #
###############################################################################

# glock
# ak_47
# shotgun
# hunting_rifle

###############################################################################
# DRUGS                                                                       #
###############################################################################

advil = Consumable('Advil', 'advil', 15, 15, advil_use)
# tylenol (should hurt you if you have any alcohol stacks)
weed = Consumable('Weed', 'weed', 25, 75, weed_use)
coke = Consumable('Coke', 'coke', 50, 50, coke_use)
# beer (1 alcohol stack)
# wine (2 alcohol stacks)
# whiskey (3 alcohol stacks)
# fourty (4 alcohol stacks)

# player.you.inventory.append(helm)


if __name__ == '__main__':
    # DBGGR
    print("Player Max HP: ", player.you.stat_block.hp.max_val)
    print("Equipping helm.")
    player.you.equip(helm)
    print("Current head item: ", player.you.head.cur_item.name)
    print("Player Max HP: ", player.you.stat_block.hp.max_val)
    print("Unequipping helm.")
    player.you.unequip(player.you.head)
    print("Player Max HP: ", player.you.stat_block.hp.max_val)
    print(player.you.head.cur_item)
