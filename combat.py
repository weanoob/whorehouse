"""
This module handles the game's combat system. All combats are one on
one, always the player versus the enemy. The enemy always goes first.
This is probably going to always be under construction.
"""

from random import randint
from spells import all_spells

import player
import items


class TempMod:
    # these are like buffs but they only last for the duration of combat

    def __init__(self, stat, amount):
        # stat needs to be a string, we use this to make it get the target's stat
        self.stat = stat
        self.amount = amount

    def toggle(self, target, mode):
        # first get the stat we want to change
        stat_to_change = target.stat_block.stats_by_nick[self.stat]

        if mode == 1:
            if target == player.you:
                player_mods.append(self)
            else:
                enemy_mods.append(self)
        else:
            if target == player.you and self in player_mods:
                player_mods.remove(self)
            elif self in enemy_mods:
                enemy_mods.remove(self)
            else:
                print("ERROR!")
                print("self.stat = %s" % self.stat)
                print("self.amount = %s" % self.amount)
                print("target = %s" % target)
                print("mode = %s" % mode)

        stat_to_change.mod(self.amount, mode)


class Fighting:
    # this is our global var to see if we're in combat because I don't
    # see a better way to do this than through a global var
    # todo make this not a class
    in_combat = False 

# more globals
enemy_mods = []
player_mods = []


def start_combat(enemy):
    # this function starts combat and makes it continue until it ends
    # Fighting.in_combat should only ever be true when this function starts and 
    # right before it end
    Fighting.in_combat = True
    print("You've engaged %s!" % enemy.name)
    while Fighting.in_combat is True:
        print("Enemy HP:", enemy.stat_block.hp.val)  # DBGGR
        # extra_turn always sets to true at the start of the loop to
        # let the player take his turn, then resets to false once the
        # player has taken a legitimate turn
        extra_turn = True
        enemy_action(enemy)  # this is the enemy's move, the enemy always goes first
        
        # this is the player's move
        # the extra_turn var and subsequent while loop are to ensure that
        # the player's turn is not wasted when asking for help or when
        # getting a KeyError on accident
        while extra_turn is True:
            extra_turn = combat_parser({}, enemy)

    # clear temp mods
        

def enemy_action(enemy):
    # this picks a random move from the moveset and makes the enemy
    # use that ability
    move_used = randint(0, len(enemy.stat_block.moveset) - 1)
    enemy.stat_block.moveset[move_used](enemy)

###############################################################################
# TEMPMODS                                                                    #
###############################################################################

hollar_mod = TempMod('arm', -50)

###############################################################################
# PLAYER'S ACTIONS                                                            #
###############################################################################


def combat_parser(extra_verbs, foe):
    # this parser is used in combat

    # figure out a way to do this without extra_verbs

    initial_list = {'help': combat_help,
                    'attack': normal_attack,
                    'flee': flee,
                    'use': use_item,
                    'cast': cast_spell}
    
    # make initial_list update with new moves the player learns later

    print("It is your turn.")
    stuff = input('>')
    if stuff == '':
        print('Please enter a command.')
        return True
    stuff = stuff.casefold()
    stuff_split = stuff.split()
    
    if stuff_split[0] in initial_list:
        extra_turn = initial_list[stuff_split[0]](stuff_split, foe)
        # this is so we don't use up our turn asking for help
        # see start_combat() for more details
        return extra_turn
    else:
        print("You can't do that!")
        return True


def cast_spell(input_list, foe):
    if input_list[1] in all_spells:
        all_spells[input_list[1]].cast(foe)
    else:
        print("You don't know a spell like that!")
        return True


def combat_help(input_list, foe):
    if len(input_list) == 1:
        print("Use the following commands:")  
        print('"help [action]" to see what that action does')
        print('"attack" to attack the enemy with a normal attack')
        print('"cast [ability]" to cast a spell or use a special technique')
        print('"flee" to try to run away from battle')
        print('"use [item]" to use an item in your inventory')

    return True


def flee(input_list, foe):
    flee_chance = randint(1, 2)
    if flee_chance > 1:
        print("You got away!")
        Fighting.in_combat = False
    else:
        print("The enemy blocked your path!")


def normal_attack(input_list, foe):
    # this is the player's basic attack: if the player's hit roll hits, they
    # deal damage to the enemy
    
    # input list isn't used here other than to satisfy the interpreter
    
    hit_roll = randint(1, 100)
    player_stats = player.you.stat_block
    
    chance_to_hit = player_stats.acc.val - foe.stat_block.arm.val
    
    if chance_to_hit >= hit_roll:
        dealt_dmg = damage(foe, fluctuate(player_stats.atk.val, 50))
        print("You hit %s for %i damage!" % (foe.name, dealt_dmg))
        check_dead(foe)
    else:
        print("You miss %s!" % foe.name)


def use_item(input_list, foe):
    pass

###############################################################################
# ENEMY ACTIONS                                                               #
###############################################################################


def basic_attack(attacker):
    # rolls an attack roll, if the attack roll is greater than or equal to the
    # chance to hit, the player takes damage, otherwise nothing happens
    hit_roll = randint(1, 100)
    player_stats = player.you.stat_block
    
    chance_to_hit = attacker.stat_block.acc.val - player_stats.arm.val
    
    if chance_to_hit >= hit_roll:
        dealt_damage = damage(player.you, fluctuate(attacker.stat_block.atk.val, 50))  # deal damage to the player
        print("%s hits you for %i damage!" % (attacker.name.title(),
                                              dealt_damage))
        check_dead(player.you)  # check if player is dead
        print("You have %i HP left!" % player_stats.hp.val)
    else:
        print("%s misses you!" % (attacker.name.title()))


def cry(attacker):
    print('%s cries.' % (attacker.name.title()))


def whine(attacker):
    print('%s asks while sobbing, "Why are you doing this?"' % (attacker.name.title()))


def hollar(attacker):
    print('%s hollars at you incoherently.' % attacker.name)
    print(player.you.stat_block.arm.val)
    hollar_mod.toggle(player.you, 1)
    print(player.you.stat_block.arm.val)

    # # 50% chance of reducing armor, 50% chance of no effect
    # if randint(0, 1) == 1:
    #     # make temporary mods
    #     pass
    # else:
    #     print("You are completely unfazed by it.")
    
###############################################################################
# MISC COMBAT FUNCTIONS                                                       #
###############################################################################


def damage(target, dmg):
    # This function deals dmg damage to the target.
    # ALWAYS pass in fluctuate()ed damage.
    target.stat_block.hp.val -= dmg
    return dmg


def check_dead(target):
    # always call this function immediately after someone takes damage
    # this function checks if someone is dead yet

    # if YOU die
    if target == player.you and target.stat_block.hp.val <= 0:
        print("YOU'RE DEAD! GGWP")
        input("Press any key to continue.")
        exit(0)

    # if the enemy dies
    elif target.stat_block.hp.val <= 0:  # if an enemy died
        Fighting.in_combat = False
        print("You defeated %s!" % target.name)
        if target.is_random is True:
            target.reset()
        loot_enemy(target)


def loot_enemy(target):
    # This is what loots the enemy when the target is killed
    # First, the items that are looted are put into looted_items. Then,
    # we make another list of the items in looted_items, but with just
    # one of those items. Finally, we use both of these to print out an
    # accurate count of each item.

    # first, make sure the target HAS some loot tables
    if target.loot_list is None or len(target.loot_list) == 0:
        return

    # get all of the looted items into looted_items
    looted_items = []

    print("You loot:")
    for i in range(0, len(target.loot_list)):
        looted_items += target.loot_list[i].begin_looting([])

    # make a second list, one_of_each, without any duplicates
    # at some point, this should become a function in player.py
    one_of_each = strip_duplicates(looted_items)

    # iterate through the items, then display a count of each
    # try to get these to alphabetize later
    for j in range(0, len(one_of_each)):
        print(one_of_each[j].name, ': ', looted_items.count(one_of_each[j]))

    # finally, add the items to the player's inventory
    for k in range(0, len(looted_items)):
        player.you.inventory.append(looted_items[k])


def strip_duplicates(list_of_loot):
    stripped_loot = []
    for i in range(0, len(list_of_loot)):
        if list_of_loot[i] not in stripped_loot:
            stripped_loot.append(list_of_loot[i])

    return stripped_loot


def fluctuate(num, div):
    # return a random number that is between
    # (100% - div%) and (100% + div%)
    # in other words, input a number and a percentage, and it
    # returns a random number within div% of that number
    
    if div > 100:
        print("ERROR: div can't go above 100, setting to 100")
        div = 100
    elif div < 0:
        print("ERROR: div can't go below 0, setting to 0")
        div = 0
    
    frac = num * (div / 100)
    num -= frac
    
    num += randint(0, (frac * 2))
    return int(num)

if __name__ == '__main__':
    # test combat out with a dindu
    import actors
    actors.dindu.fight()
