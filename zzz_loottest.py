"""
This is a special file for testing if looting works the right way.
It will perform loot 100 times and report statistics about how many
times each item dropped on a particular loot table.
"""

import loottables
import player

i = 0
times_to_test = 1

print("Rolling %i times." % times_to_test)

while i < times_to_test:
    i += 1
    # noinspection PyArgumentList
    loottables.test_norm.begin_looting()

total_weed = 0
total_coke = 0

for each in range(0, len(player.you.inventory)):
    if player.you.inventory[each].nick == 'weed':
        total_weed += 1
    elif player.you.inventory[each].nick == 'coke':
        total_coke += 1

print("Weed: ", total_weed)
print("Coke: ", total_coke)
print("$: ", player.you.cash)
