"""
This is the file for Stats, Statblocks, and related functions.

A Stat is an individual

HP is the amount of damage you can take before death.
Atk is the amount of damage you do
Acc is how accurate you are
Arm is how well you can avoid attacks
moveset is only used by NPCs
Mana is how many spells you can cast
"""


class Stat:
    def __init__(self, statname, val, level_up_val):
        self.statname = statname
        self.val = val
        self.level_up_val = level_up_val

    def level_up_increase(self):
        self.val += self.level_up_val
        print("Your %s has gone up by %s!" % (self.statname, self.level_up_val))

    def mod(self, mod_val, mode):
        # used when equipping items to modify stat values
        # also used by buffs and stuff
        # if mode is 0, the value is subtracted instead of added
        # mode should only be true when equip()ing
        # and only be false when unequip()ing
        if mode == 0:
            mod_val = -mod_val

        self.val += mod_val


class CappedStat(Stat):
    def __init__(self, statname, val, level_up_val):
        Stat.__init__(self, statname, val, level_up_val)
        self.max_val = self.val

    def level_up_increase(self):
        super().level_up_increase()
        self.max_val += self.level_up_val

    def mod(self, mod_val, mode):
        super().mod(mod_val, mode)
        if mode == 0:
            mod_val = -mod_val

        self.max_val += mod_val


class StatBlock:
    # these are the statblocks that all actors use, this is done this way so we
    # don't have really really long __init__ functions and definitions
    def __init__(self, hp, atk, acc, arm, moveset):
        self.hp = CappedStat('hitpoints', hp, 10)
        self.atk = Stat('attack', atk, 3)
        self.acc = Stat('accuracy', acc, 5)
        self.arm = Stat('armor', arm, 5)
        self.moveset = moveset
        self.stats_by_nick = {
            'hp': self.hp,
            'atk': self.atk,
            'acc': self.acc,
            'arm': self.arm,
        }


class PlayerStatBlock(StatBlock):
    # this will later be changed so that all of the player's special
    # stats like nut and endurance can be changed
    # noinspection PyMissingConstructor
    def __init__(self):
        self.hp = CappedStat('hitpoints', 100, 10)
        self.atk = Stat('attack', 10, 3)
        self.acc = Stat('accuracy', 85, 5)
        self.arm = Stat('armor', 0, 5)
        self.moveset = None
        self.nut = CappedStat('nut', 3, 0.5)
        self.endurance = CappedStat('endurance', 100, 10)
        self.mp = CappedStat('mana', 100, 10)
        self.stats_by_nick = {
            'hp': self.hp,
            'atk': self.atk,
            'acc': self.acc,
            'arm': self.arm,
            'nut': self.nut,
            'end': self.endurance,
            'mp': self.mp
        }

if __name__ == '__main__':
    test_block = StatBlock(0, 0, 0, 0, None)
    test_block.hp.val += 1
    print(test_block.hp.val)
    print(test_block.stats_by_nick['hp'].val)
