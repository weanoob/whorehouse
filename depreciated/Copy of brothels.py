""" todo
- make the menu more like how a menu should work
- make diseases spreadable
- add a source for income (fighting hobos?)
- TURN THE INFECTIONS INTO FUNCTIONS OF WHORES
"""

from random import randint

# global vars
global cash, player_std, ho_menu
cash = 10000
player_std = []
ho_menu = []  # this is defined after we define whores


# these functions are the main loop of functions


def show_all_hoes():
    # prints all hoes' names
    print("Today's hoes:")
    for c in range(0, len(ho_menu)):
        print(ho_menu[c].name)
    print()  # for new line


def ho_chooser():
    # this shit asks what ho we want
    ho = input('Go ahead, choose a ho!\n')

    # this takes our input, compares it to each whore's name, and tries to find
    # a match. If one exists, it then shows that whore's specs.
    chosen = False
    for d in range(0, len(ho_menu)):
        if ho.lower() == ho_menu[d].name.lower():
            chosen = True
            ho_menu[d].show_specs()
            # this is our workaround to change the diseases globally
            do_or_dont(ho_menu[d])
    if not chosen:  # this and "chosen" exist as a makeshift "else" outside the for loop
        print("Sorry, that ho doesn't exist!")
        return [0]  # this is done to keep looping in our main loop


def do_or_dont(star_ho):
    print("Do you want to fuck %s? Y/N" % star_ho.name)
    consent = yes_or_no()
    if consent:
        paid = payment(star_ho.price)
        if paid == 1:  # fuck_ho(star_ho)
            fuck_ho(star_ho)
    else:
        return  # ([0]) # if no, this is done to keep looping in our main loop


# action 1: fuck_ho()


def fuck_ho(your_ho):
    # theoretically self explanatory
    # however, condom breakage is calculated here
    # also, we return EVERYTHING to global here in order to modify the instances
    # without having to deal with stupid rules about functions
    print("Do you want to use a condom? Y/N")
    condom = yes_or_no()
    if condom:
        break_chance = randint(1, 100)
        print("You fuck %s! You fuck her good!" % your_ho.name)
        if break_chance < 5:
            condom = False
            print("Uh oh, the condom broke!")
        your_ho.infect_player(player_std, condom)
        your_ho.get_infected_by_player(player_std, condom)
    else:
        print("You fuck %s! You fuck her good!" % your_ho.name)
        your_ho.infect_player(player_std, condom)
        your_ho.get_infected_by_player(player_std, condom)


# def disease_infect(infectee_1, infectee_2, condom_on):
#     # infectees should always be a Whore.disease or player diseases
#     # condom_on should always be a bool
#     # unless an infection is one-way, this function should ALWAYS be repeated
#     # afterwards with the infectee args switched
#     # either way, we always return infectee_2
#     
#     #first, we check if infectee_1 has any diseases
#     if len(infectee_1) == 0:
#         return(infectee_2)
#     
#     # next, we loop through infectee_1's diseases
#     for i in range(0, len(infectee_1)):
#         # check to see if infectee_2's already has the disease
#         have_disease = False # reset our control var
#         have_disease = check_same_disease(infectee_1[i], infectee_2)
#         
#         # skip if we have the disease
#         if have_disease != True:
#             # roll to see if we get infected
#             infected = infect_roll(infectee_1[i], condom_on)
#             # if so, append the infectee_1's disease
#             if infected == True:
#                 # linus torvalds can kill himself instead for all I care
#                 infectee_2.append(infectee_1[i])
#                 print("Uh oh! You caught %s!" % (infectee_1[i].name))
#     
#     # and now we're done!
#     return(infectee_2)


def check_same_disease(infecter, infectee):
    # checks to see if the infecter and infectee already have the same disease
    if len(infectee) == 0:
        return False
    for i in range(0, len(infectee)):
        if infecter == infectee[i]:
            return True
    return False  # if our for loop didn't return true


def infect_roll(disease_rolled, condom_status):
    roll_of_fate = randint(1, 100)
    if disease_rolled.condom_protects == True and condom_status == True:
        return False  # don't infect if condom is on and condom protects
    else:
        pass

    if roll_of_fate <= disease_rolled.inf_chance:
        return True
    else:
        return False


# These functions aren't in a particular order for any reason    


def find_ho(ho_to_find, menu):
    # this should be used by the main module to retrive the ho we want that we 
    # want so we can manipulate them in the main function, THEORETICALL
    # earlier in the whorehouse.show() function, I tried to pass in the whore
    # with brothels.tell[input_list[1]].show_specs(), but it didn't like that
    # since there's nothing named 'tell' in here. So, let's hope this works.
    # menu should ALWAYS be brothels.ho_menu
    for n in range(0, len(menu)):
        if menu[n].name.casefold() == ho_to_find.casefold():
            return menu[n]


def yes_or_no():
    kl = 1
    while kl == 1:  # make this a function like yes_or_no()?
        yn = input()
        if yn.lower() == 'y':
            return True  # if yes
        elif yn.lower() == 'n':
            return False  # if no
        else:
            print('Please input "Y" or "N".')


# def payment(price):
#     # this function checks if you have enough money to buy something
#     # and blocks the purchase if you don't
#     global cash
#     if cash >= price:
#         cash -= price
#         show_cash()
#         return(1)
#     else:
#         print("You don't have enough money for that!")
#         return(0)

# def show_cash():
#     print("You have $%.2f" % cash)    


def price_calc(a, hT, b, w, h, d):
    hT -= 125
    b -= 60
    w -= 45
    h -= 55

    subtotal = 150  # start at $150
    subtotal += b * 3  # add bust size price
    subtotal = subtotal - (w * 0.3) - (h * 0.3) - (hT * 0.1)  # subtract things
    if a < 18:  # underage hookers are illegal and thus more expensive
        subtotal *= 1 + ((18 - a) * 0.2)
    else:  # otherwise hookers get less valuable with age
        subtotal -= (a - 18) * 15

    if len(d) > 0:  # disesased hookers half off!
        subtotal /= 2
    return subtotal  # finish this later


class Whore:
    def __init__(self, name, age, height, bust, waist, hip, disease):
        self.name = name
        self.age = age
        self.height = height  # should be at least 125
        self.bust = bust  # should be at least 60
        self.waist = waist  # should be at least 45
        self.hip = hip  # should be at least 55
        self.disease = disease
        self.price = price_calc(age, height, bust, waist, hip, disease)

    def show_specs(self):
        print('Name:\t', self.name)
        print('Age:\t', self.age)
        print('Height:\t', self.height, 'cm')
        print('Bust:\t', self.bust, 'cm')
        print('Waist:\t', self.waist, 'cm')
        print('Hip:\t', self.hip, 'cm')
        print('Price:\t$%.2f' % self.price)  # http://www.python-course.eu/python3_formatted_output.php

    def infect_player(self, player_dis, condom_on):
        # this function appends player_std with any of the Whore's Diseases if
        # the infect chance is low enough
        # check if we even have any diseases
        if len(self.disease) == 0:
            return

        for i in range(0, len(self.disease)):
            # check to see if infectee_2's already has the disease
            # if so, roll the infect roll, and infect if it's low enough
            if check_same_disease(self.disease[i], player_dis) == False and \
                            infect_roll(self.disease[i], condom_on) == True:
                player_dis.append(self.disease[i])
                print("Uh oh! You caught %s!" % self.disease[i].name)  # DBGGR

        return player_dis

    def get_infected_by_player(self, player_dis, condom_on):
        # this basically does the same thing as infect_player but backwards
        # it gives Player disease to the Whore

        if len(player_dis) == 0:
            return

        for i in range(0, len(player_dis)):
            if check_same_disease(player_dis[i], self.disease) == False and \
                            infect_roll(player_dis[i], condom_on) == True:
                self.disease.append(player_dis[i])
                print("%s caught %s" % (self.name, player_dis[i].name))  # DBGGR
        return


class Disease:  # these are STDs you can catch
    def __init__(self, name, inf_chance, condom_protects, damage, cure_cost):
        self.name = name
        self.inf_chance = inf_chance
        self.condom_protects = condom_protects
        self.damage = damage
        self.cure_cost = cure_cost


# define STDs before whores so whores can have STDs

testitis = Disease('testitis', 100, False, 100, None)  # this disease is used for debugging
super_testitis = Disease('super testitis', 100, False, 100, None)  # another disease used for debugging
herpes = Disease('herpes', 45, False, 12, None)
aids = Disease('AIDS', 75, True, 2, None)
chlamidya = Disease('chlamidya', 66, True, 6, None)

player_std.append(testitis)  # DBGGR

cassie = Whore('Cassie', 18, 145, 80, 55, 82, [])
jane = Whore('Jane', 22, 162, 85, 57, 78, [super_testitis])
loranda = Whore('Loranda', 25, 156, 92, 66, 85, [])

ho_menu = [cassie, jane, loranda]

show_cash()  # print("You have $", cash)

# this is out main loop, which can't be its own function
# keep_looping = 1
# while keep_looping == 1:
#     action = ho_chooser()
