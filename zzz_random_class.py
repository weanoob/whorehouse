# this is a prototype of randomly generated actors

import actors
from random import choice

def random_name():
    names = ['Ashley', 'Barbara', 'Cassie', 'Denise', 'Emily', 'Faith']
    return choice(names)

my_dict = {}

for i in range (0, 3):
    x_name = random_name()
    # noinspection PyArgumentList
    my_dict[x_name] = actors.Actor(x_name, actors.whore_stats, [], actors.male_holes, None)
    print(my_dict[x_name].name)

print(my_dict)