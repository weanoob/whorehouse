"""
This is just a bunch of functions that don't fit anywhere else.
"""


def check_all_same_type(l):
    # checks to make sure all items in list (or tuple) are the same type
    # returns True if they are, False if they're not
    first_type = type(l[0])
    for i in range(1, len(l)):
        if isinstance(l[i], first_type) is False:
            return False

    return True


def make_list_into_attr(l, attr):
    # Takes a list of objects and returns a new list with each value being replaced with the attr attribute
    # of that object
    # l is a list of integers
    # attr is the attribute you want to turn the values of the list into

    if check_all_same_type(l) is False:
        print("The values in this list do not have the same type.")
        exit(1)

    for i in range(0, len(l)):
        l[i] = getattr(l[i], attr)

    return l


def ordinalize(num):
    ns = str(num)
    if ns[-1] == '1':
        o = 'st'
    elif ns[-1] == '2':
        o = 'nd'
    elif ns[-1] == '3':
        o = 'rd'
    else:
        o = 'th'
    return '%s%s' % (num, o)


def yes_or_no():
    # this is in the player module because there isn't really
    # a better place to put it, so we'll just tie
    while True:
        yn = input('>')
        if yn.lower() in ['y', 'yes']:
            return True  # if yes
        elif yn.lower() in ['n', 'no']:
            return False  # if no
        else:
            print('Please input "Y" or "N".')

if __name__ == '__main__':
    print(ordinalize(input('>')))
