def check_zone_collision(coords_list):
    # checks to make sure no zones overlap
    # this may be buggy, and it only works with perfect rectangles for now


    # THIS LOOP is for iterating through what we compare WITH
    for i in range(0, len(coords_list)):
        first_compare = coords_list[i]

        # THIS LOOP is for iterating through what we compare TO
        for j in range(i + 1, len(coords_list)):
            second_compare = coords_list[j]

            if xy_checker(first_compare, second_compare):
                return True

    return False


def xy_checker(comp_1, comp_2):
    # figures out if we're just sharing a line or if we're actually colliding

    # i've calculated that we only actually have a collision if there's something
    # in both of these tuples
    x_shared = tuple(set(comp_1[0]) & set(comp_2[0]))
    y_shared = tuple(set(comp_1[1]) & set(comp_2[1]))

    if len(x_shared) == 0 or len(y_shared) == 0:
        return False
    else:
        return True

x = (
    (range(0, 3), range(0, 3)),
    (range(0, 3), range(3, 5)),
    (range(0, 10), range(0, 12))
    )

print(check_zone_collision(x))
