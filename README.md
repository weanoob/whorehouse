# README #

This is a repo for a text-based game I'm calling Whorehouse. The goal of the game is to explore the hood and fuck whores.
Also I have no idea how to use Bitbucket so have mercy upon me.

If you're reading this I want you to critique my code.

### What is this repository for? ###

* A shitty text-based game I am making to improve my programming skills
* Version 0.1.11
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Run Whorehouse.py in your terminal
* That's it

### Contribution guidelines ###

* Don't
* Just tell me what parts of my code are bad
* Also tell me what could be better

### Who do I talk to? ###

* Me

### Planned Features ###

* Storyline
* Actual content

### Special Thanks ###

* RevanProdigalKnight for showing me proper OOP in attributes.py

## Changelog ##

v 0.1.11 4/29/16

* Added Shops to scenes.py
* Moved yes_or_no() to utils.py
* Stats are no longer an integer, they're now a Stat object with their own methods for using temp buffs from mods and
equipment and the like
    * CappedStats are a replacement for having to have a stat with a max and its actual value (like hp and max_hp)

v 0.1.10 4/17/16

* Nut per square changed to 0.1 (down from 0.2)
* Added a special print for player.you.print_current_position() used for printing position while in a Scene
* Added Zones
    * Need to add zone checking
* Added boundaries (just checks if where a player is moving is in a Zone or not)
* Added pregame testing for errors (mostly for dev stuff)

v 0.1.9.1 4/10/16

* No longer tracking changelog.txt on git

v 0.1.9 4/10/16

* Moving with verb_go() now shows you coordinates in the form of street names. "Street" is your X coordinate, "Road" is
your Y coordinate. "Main Street" is 0 for X, "Hillary Clinton Road" is 0 for Y.
* Fixed a bug in combat.py, blank commands won't crash the game now
* Maybe forgot some other shit?

v 0.1.8 4/7/16

* Added show_stats
* Fixed bug in fucking.py, nut should show as an int after fucking something
* Added utils.py (I may never use this)
* Removed a try-except clause in scenes.py

v 0.1.7 forgot date

* Fixed nut display

v 0.1.6 3/24/2016

* Made it so that you don't ALWAYS get an encounter when traveling
* Added random encounters and nut restoration with verb_go()
* Added 'show stats' and 'show cash', usable anywhere

v 0.1.5 3/23/2016

* Added known_location and the like

v 0.1.4 3/21/2016

* Spells now cost stuff
* temp_mod is now mod_stat and can be used by anything
* Added tile-by-tile movement (verb_go())
    * this is not fully implemented

v 0.1.3 3/19/2016

* Added spells
* Added mana and known_spells to player.py

v 0.1.2 1/15/2016

* Fixed bug in input_parser that would cause a crash upon entering nothing
* Added hospital HP restoration

v 0.1.1 1/13/2016

* Made all actors (so far) have random diseases
* Started tracking date on updates

v 0.1.0

* Started tracking versions
* Removed RandomActor child class, all Actors now have an is_random variable
* disease variable in Actor is no longer a list of Diseases, instead it can either be an int or a list
    * If it's an int, it adds random diseases based on how high the int is
    * If it's a list of diseases, it simply adds those diseases
