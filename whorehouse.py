###############################################################################
#                                                                             #
###############################################################################
# use above as template for organizing

import scenes
import input_parser
import player
import items

verb_list = {'goto': scenes.verb_goto,
             'go': scenes.verb_go,
             'help': scenes.verb_help,
             'use': items.verb_use,
             'show': scenes.verb_show}

###############################################################################
# PREGAME TESTING STUFF                                                       #
###############################################################################

# all of this shit is to test things before the game starts

# make sure zones don't collide
# DBGGR
if scenes.check_zone_collision(scenes.zones):
    print("Zones are colliding. If this becomes a problem, update this to show where.")
    exit(1)

player.you.current_zone = scenes.hood

# sets our starting scene to Joe's Fuck Shack
scenes.joes.enter_scene(dont_print=1)
# makes it so that we always know where Joe's Fuck Shack is
player.you.known_locations.append('joes')

print("Welcome to Whorehouse! Your name is John, and you have $%.2f in pocket." % player.you.cash)
print("Your job is to explore the city, fuck bitches, get money, and stay alive.")
print("You're currently standing outside of Joe's Fuck Shack. Try fucking some hoes.")
print("Type 'help' if you have any questions! Good luck!")

while True:
    # verb_list is in our main loop because update()ing it 
    input_parser.parse_input(verb_list)
