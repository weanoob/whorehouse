"""
This lists all of the spells in the game. Each spell is a class with a
main function, called main_func. This function does all of the main stuff.

cost is a nested list with all of the costs for the spell. Usually, this
will be mp, but sometimes hp or nut. Example: [['mp', 10], ['hp', 20]]
"""

import player
import combat

# this is an important dict used to find spells by their nick
# ALWAYS add a spell to a dict after instatiating: {spell.nick: spell}
all_spells = {}


class Spell:

    # todo add cooldowns

    def __init__(self, name, nick, main_func, cost):
        self.name = name
        self.nick = nick
        self.main_func = main_func
        self.cost = cost
        all_spells[self.nick] = self

    def learn(self):
        if self not in player.you.known_spells:
            player.you.known_spells.append(self.name)

    def check_if_castable(self):
        # return False if we can cast, return True if we can't
        for i in range(0, len(self.cost)):
            stat = self.cost[i][0]
            # if the cost is greater than the current value, stop casting
            if self.cost[i][1] > stat.val:
                return True
        return False

    def cast(self, target):
        # make sure you know the spell
        if self.name not in player.you.known_spells:
            print("That's not a spell you know!")
            # return stuff to make you not skip your turn
            return

        # make sure you have enough of each resource
        if self.check_if_castable() is not False:
            # CHANGE THIS TO TELL YOU WHAT STATS YOU DON'T HAVE ENOUGH OF
            # hint: change check_if_castable to return a list of the stats
            # that you're lacking
            print("You can't cast that!")
            # return stuff to make you not skip your turn
            return

        # this subtracts mp, hp, nut, etc. from our stats
        for i in range(0, len(self.cost)):
            self.cost[i][0].val -= self.cost[i][1]

        self.main_func(target)

###############################################################################
# SPELL MAIN_FUNCS                                                            #
###############################################################################


def insta_kill_main_func(target):
    # used for testing only
    # deal damage to the target equal to the target's hp
    combat.damage(target, target.stat_block.hp.max_val)
    combat.check_dead(target)

###############################################################################
# SPELL MAIN_FUNCS                                                            #
###############################################################################


# def string_to_stat(s):
#     if s == 'mp':
#         return player.you.stat_block.mp.val
#     elif s == 'hp':
#         return player.you.stat_block.hp.val
#     elif s == 'nut':
#         return player.you.stat_block.nut.val
#     else:
#         print("SOMETHING IS HORRIBLY WRONG AND %s ISN'T A STAT" % s)

###############################################################################
# SPELL CLASS INSTANTIATIONS                                                  #
###############################################################################

insta_kill = Spell('Instant Kill Technique', 'instakill', insta_kill_main_func, [[player.you.stat_block.mp, 10]])
# todo make these
# nut_shot # (make it say "THAT'S MY PURSE!" and "I DON'T KNOW YOU!")
# heal
# std_blood_blast
# magic_missile
# curse (reduce defense)
# blind
# fireball

# DBGGR
if __name__ == '__main__':
    import actors
    # fight a dindu and try to kill him
    insta_kill.learn()
    actors.dindu.fight()
