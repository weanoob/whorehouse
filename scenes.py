"""
This file is for all Scenes in the game and related things, such as verbs.

Any arguments in commands is in addition to commands in a scene. For example,
'fuck' and 'show' are always in Brothels.

todo: flesh out verb_buy
"""

import encounters
import player
import math
import actors
import items
import utils


class Scene:
    # Scenes are like rooms, you can only be in one scene at a time.
    # The global variable Current.player.you.in_scene sets the room you're in.
    #
    # Coords are used by verb_goto() to calculate the distance from your current
    # position to your destination, adding random encounters as appropriate.
    # They're always a two-length tuple, representing X and Y.
    #
    # Commands are dictionaries that are always updated into parse_input()'s
    # verb_list. This is done so that only certain functions can be done in
    # certain places.
    def __init__(self, name, nick, coords, intro, commands):
        self.name = name
        self.nick = nick  # nickname used by the game
        self.coords = coords  # where the scene is on the map
        self.intro = intro  # this is what prints upon entering the scene
        self.commands = commands  # commands you can use in the scene, such as fuck
        self.add_data()

    def enter_scene(self, snap=0, dont_print=0):
        # if snap is 1, then we move the player's coordinates to the new scene
        if snap == 1:
            player.you.current_position = list(self.coords)

        # if dont_print is 0, print the intro
        if dont_print == 0:
            print(self.intro)

    def add_data(self):
        # this adds the scene to the following dicts/lists:
        # scenes_by_nick, scenes, scenes_by_coords
        # this is the only time these will be used as globals
        scenes.append(self)
        scenes_by_nick[self.nick] = self
        scenes_by_coords[self.coords] = self


class Brothel(Scene):
    # Brothels are Scenes, but they have menus and ways to manipulate Whores
    def __init__(self, name, nick, coords, intro, commands, menu):
        super().__init__(name, nick, coords, intro, commands)
        self.commands.update({'show': verb_show, 'fuck': verb_fuck})
        self.menu = menu

    def show_all_hoes(self):
        # prints all hoes' names
        print("Today's hoes:")
        for c in range(0, len(self.menu)):
            print(self.menu[c].name)

    def find_ho(self, ho_to_find):
        # this should be used by the main module to retrieve the ho we want that we
        # want so we can manipulate them in the main function, THEORETICALLY
        # earlier in the whorehouse.show() function, I tried to pass in the whore
        # with brothels.tell[input_list[1]].show_specs(), but it didn't like that
        # since there's nothing named 'tell' in here. So, let's hope this works.
        # menu should ALWAYS be brothels.ho_menu
        for n in range(0, len(self.menu)):
            if self.menu[n].name.casefold() == ho_to_find.casefold():
                return self.menu[n]
        # else
        print("Sorry, that ho doesn't exist!")
        return None


class Shop(Scene):
    # Shops are scenes where you can buy things
    # Each Shop has a Menu, which is has a list of MenuElements, which are the things you buy
    def __init__(self, name, nick, coords, intro, commands, menu):
        super().__init__(name, nick, coords, intro, commands)
        self.menu = menu
        self.commands.update({'show': verb_show, 'buy': verb_buy})
        # show will be used to show the menu as well as the description of individual items


class Menu:
    def __init__(self, elements):
        # items is a list of MenuElements
        self.elements = elements

    def show_menu(self):
        for i in range(0, len(self.elements)):
            print('%s (%s): $%.2f' % (self.elements[i].merchandise.name, self.elements[i].merchandise.nick, self.elements[i].final_price))


class MenuElement:
    # merchandise is the Item or Whore for sale
    # price_mod is the markup (or discount, if negative) applied by the seller of the item
    # final_price is the price of the item after price_mod is applied
    def __init__(self, merchandise, price_mod):
        self.merchandise = merchandise
        self.price_mod = price_mod
        self.final_price = merchandise.price + price_mod


class Zone:
    # this is sort of a "meta-scene" used to figure out what kind of random encounters we come across
    # note that self.coords is NOT a fixed pair but rather two (or more?) ranges
    # we may need to attach encounter sets to this, and POSSIBLY even move it into encounters.py
    def __init__(self, name, nick, coords, intro):
        self.name = name
        self.nick = nick
        self.coords = coords
        self.intro = intro
        self.add_data()

    def add_data(self):
        # this adds the scene to the following dicts/lists:
        # scenes_by_nick, scenes, scenes_by_coords
        # this is the only time these will be used as globals
        zones.append(self)


###############################################################################
# GLOBAL VARS                                                                 #
###############################################################################

# make a function that automatically makes this in each Scene
scenes = []
scenes_by_nick = {}
scenes_by_coords = {}
zones = []


###############################################################################
# VERB FUNCTIONS                                                              #
###############################################################################

# Verbs are functions that basically take the player's input from input_parser.parse_input
# and manipulate the latter half of the input, the non-verb portion. This is
# usually done by matching the latter half of input to a dictionary's key, then
# calling the function corresponding to that key's value.


def verb_help(input_list, commands):
    # this is the help function, it checks if you just entered "help"
    # or "help ____", if you entered something in ____, it gives help
    # on the thing you entered in

    if len(input_list) == 1:
        print("These are some simple commands. Type 'help [command]' to learn how to use them:")
        for key in commands:
            print('"%s"' % key)
        return True

    # CHANGE THIS TO A DICTIONARY WHEN YOU REMEMBER HOW TO CODE
    if input_list[1] == 'help':
        print("Stop being a smartass.")
    elif input_list[1] == 'go':
        print("Go one block in one of the following directions:\n'north'\n'south'\n'east'\n'west'")
    elif input_list[1] == 'goto':
        print("Go to the place specified. Know the nickname of your destination.")
        print("For example, 'goto hospital' will send you to the hospital.")
        print("Beware, for dindus, hobos, and streetwalkers wander the streets...")
    elif input_list[1] == 'use':
        print("Use something, usually an item in your inventory. ")
    elif input_list[1] == 'show':
        print("This shows you something, such as 'hoes' in a brothel,")
        print("or your 'inv'entory. There may be other things you can")
        print("be shown!")
    elif input_list[1] == 'fuck':
        print("Fuck someone or something. If you can. This does not")
        print("allow you to rape someone. While fucking, you play a minigame")
        print("where you try to last as long as possible. Different holes")
        print("have different amounts of pleasure.")
    elif input_list[1] == 'heal':
        print("Lets you heal your damage at a hospital. The normal rate is $3")
        print("per point of damage.")
    elif input_list[1] == 'cure':
        print("Lets you cure your STDs. The price changes depending on the STD.")
        print("Some STDs can't be cured.")


def verb_go(input_list, sbc=scenes_by_coords):
    # this puts you exactly one tile north, west, east, or south

    if len(input_list) == 1:
        incomplete_phrase(input_list)
        return

    # this is where we store where we move, it will be used as an arg
    # later to modify player.you.current_position
    movement = [0, 0]

    # this sets our movement direction
    if input_list[1] == 'north':
        movement[1] += 1
    elif input_list[1] == 'south':
        movement[1] -= 1
    elif input_list[1] == 'west':
        movement[0] -= 1
    elif input_list[1] == 'east':
        movement[0] += 1
    else:
        print('You can only go "north", "south", "east", or "west".')

    # we use this to make sure we're not moving somewhere outside of a zone before we actually move the player
    change_preview = (player.you.current_position[0] + movement[0], player.you.current_position[1] + movement[1])

    # if where the player is going isn't in a zone, don't move the player
    if check_all_scenes(change_preview) is False:
        print("Going there would send you out of bounds.")
        return

    # now we actually change the player's coordinates
    player.you.current_position[0] += movement[0]
    player.you.current_position[1] += movement[1]

    check_zone_move()

    # increase the player's nut and choose an encounter
    player.you.stat_block.nut.val += 0.1
    encounters.choose_encounter()

    # now enter the scene if there is one
    position = tuple(player.you.current_position)

    player.you.print_current_position()

    if position in sbc:
        sbc[position].enter_scene()
        if sbc[position] not in player.you.known_locations:
            make_known(sbc[position])
    else:
        player.you.print_current_position()


def verb_goto(input_list, sbn=scenes_by_nick):
    # sends you to a scene if it's possible
    # usually, you'll need to go through X random encounters in the hood
    #
    # sbn's key is the nickname of each scene, and its value is the object
    # that it represents
    # this is done the exact same way parse_input()'s verb_list works

    if len(input_list) == 1:
        incomplete_phrase(input_list)
        return

    # comment this out a lot so that you can go wherever
    if input_list[1] not in player.you.known_locations:
        print("You don't know where that is!")
        return

    current_scene = player.you.current_position

    if input_list[1] in sbn:
        target = sbn[input_list[1]]  # target is where we want to go
    else:
        print("That place doesn't exist!")
        return

    if current_scene == target.coords:
        print("You're already there!")
    else:
        # if we're not there already, we figure out how long it takes to get
        # to our destination, then we hop there step by step
        distance = calc_distance(current_scene, target.coords)
        # until you get to your destination, decrease distance by 1 and have a 
        # random encounter in the hood
        while distance > 0:
            distance -= 1
            # The player's balls slowly regenerate throughout the hood.
            player.you.stat_block.nut.val += 0.1
            encounters.choose_encounter()

        # check to see if we've changed zones and enter the new scene
        # at some point, figure out how to make this change in the middle of warping
        check_zone_move()
        target.enter_scene(snap=1)


def verb_show(input_list):
    # this function leads us into other things to show, such as hoes in a brothel
    # or the player's stats or cash
    # this was originally a Brothel-only function

    if len(input_list) == 1:
        incomplete_phrase(input_list)
        return

    # show and tell, get it?
    # but really this is a list of words we may need
    tell = {'stats': player.you.show_stats,
            'cash': player.you.show_cash}

    current_scene = get_current_scene(raise_error=0)

    # if we're in a Brothel, add these to tell
    if type(current_scene) is Brothel:
        tell['menu'] = current_scene.show_all_hoes
        tell['hoes'] = current_scene.show_all_hoes
    # else if we're in a Shop, add these to tell
    elif type(current_scene) is Shop:
        tell['menu'] = current_scene.menu.show_menu
        tell['items'] = current_scene.menu.show_menu

    # special words are for when we want abnormal behaviors with verbs
    # then we go right to the function we want
    if input_list[1] in tell:
        # if we our second word is special, then we just activate that function
        tell[input_list[1]]()
    # if we're in a brothel, try to find a ho by name
    elif type(current_scene) is Brothel:
        # we're doing it this way because we actually need to figure out
        # which ho we want, and the best way to do that is to have the
        # actors module tell us

        wanted_ho = current_scene.find_ho(input_list[1])

        if wanted_ho is not None:
            wanted_ho.show_specs()
    else:
        print("We can't show that!")


###############################################################################
# BROTHEL VERBS                                                               #
###############################################################################


def verb_fuck(input_list):
    # THIS MAY NEED TO BE REWRITTEN TO ACCOMODATE NON-WHORES
    # this function figures out what ho we want, then sends us to
    # actors.fuck_ho() with that ho as the arg

    if len(input_list) == 1:
        incomplete_phrase(input_list)
        return

    if player.you.check_nut() == 0:
        print("You don't have enough Nut to do that!")
        return ()
    current_scene = get_current_scene()
    wanted_ho = current_scene.find_ho(input_list[1])
    if wanted_ho is not None:  # if we return a Whore
        print("Do you want to fuck %s? Y/N" % wanted_ho.name)
        consent = utils.yes_or_no()
        if consent == 1:
            paid = player.you.payment(wanted_ho.price)
            if paid == 1:
                wanted_ho.fuck_ho()


###############################################################################
# HOSPITAL VERBS                                                              #
###############################################################################


def verb_heal(input_list):
    # figures out how much hp you need to heal, then charges you based
    # on how much hp you need to heal
    # Might make it so you can partially heal yourself

    if player.you.stat_block.hp.val == player.you.stat_block.hp.max_val:
        print("Your HP is at max, idiot!")
        return

    hp_to_heal = player.you.stat_block.hp.max_val - player.you.stat_block.hp.val
    heal_cost = hp_to_heal * 3

    print("You have %s out of %s hitpoints remaining."
          % (player.you.stat_block.hp.val, player.you.stat_block.hp.max_val))
    print("It will cost $%s to heal." % heal_cost)
    print("Do you want to heal? (Y/N)")

    if utils.yes_or_no() == 1:
        if player.you.payment(heal_cost) == 1:
            player.you.stat_block.hp.val = player.you.stat_block.hp.max_val
            print("You fully heal.")
    else:
        print("You decide that your wallet is more important than your"
              " injuries.")


def verb_cure(input_list):
    print("This doesn't do anything yet, hold onto your STDs!")

###############################################################################
# SHOP VERBS                                                                  #
###############################################################################


def verb_buy(input_list):
    # first we see if the item the player asked for exists on the menu
    # if it does, we ask if they want to buy it
    # if they do, we add it to their inventory and take the money

    sought_item = find_in_menu(input_list[1])

    if sought_item is False:
        print("That isn't in the store!")
        return

    print("Do you want to buy the %s?" % sought_item.merchandise.name)
    if utils.yes_or_no() is True:
        if player.you.payment(sought_item.final_price) is True:
            print("You add the %s to your inventory." % sought_item.merchandise.name)
            player.you.inventory.append(sought_item.merchandise)
    else:
        print("You don't buy it.")


def find_in_menu(target):
    # returns true if an Item with a nick of target exists in the menu
    # otherwise returns False

    current_store = scenes_by_coords[tuple(player.you.current_position)]

    for i in range(0, len(current_store.menu.elements)):
        item_to_find = current_store.menu.elements[i].merchandise.nick
        if item_to_find == target:
            return current_store.menu.elements[i]  # return the whole MenuElement

    return False

###############################################################################
# NON-VERB-RELATED FUNCTIONS                                                  #
###############################################################################


def calc_distance(pos1, pos2):
    # calculates the distance between two points
    # args should always be two Scene.coords
    # there's no need to do absolute values since we square the coords
    dist = [(pos2[0] - pos1[0]), (pos2[1] - pos1[1])]  # x2 - x1, y2 - y1
    return int(math.sqrt(dist[0] ** 2 + dist[1] ** 2))


def check_all_scenes(coords, z=zones):
    # used to check if the given coords are in any zone at all
    # returns True if we're in a zone, False if we're not

    # compares coords to all of the Zone.coords in z
    for i in range(0, len(z)):
        if check_coord_range(coords, z[i].coords):
            return True

    return False


def check_zone_collision(coords_list):
    # checks to make sure no zones overlap
    # this may be buggy, and it only works with perfect rectangles for now

    # THIS LOOP is for iterating through what we compare WITH
    for i in range(0, len(coords_list)):
        first_compare = coords_list[i].coords

        # THIS LOOP is for iterating through what we compare TO
        for j in range(i + 1, len(coords_list)):
            second_compare = coords_list[j].coords

            if xy_checker(first_compare, second_compare):
                return True

    return False


def xy_checker(comp_1, comp_2):
    # figures out if we're just sharing a line or if we're actually colliding

    # i've calculated that we only actually have a collision if there's something
    # in both of these tuples
    x_shared = tuple(set(comp_1[0]) & set(comp_2[0]))
    y_shared = tuple(set(comp_1[1]) & set(comp_2[1]))

    if len(x_shared) == 0 or len(y_shared) == 0:
        return False
    else:
        return True


def check_coord_range(coords, coord_range):
    # this checks to see if a set of coords are within a set of ranges
    # this is mostly used to check what zone we're in
    for i in range(0, 2):
        if coords[i] not in coord_range[i]:
            return False

    return True


def check_zone_move():
    # check the zone of the current position against the old zone
    # if it's a new zone, change to that zone and print the zone's welcome

    old_zone = player.you.current_zone

    for i in range(0, len(zones)):
        if player.you.current_position[0] in zones[i].coords[0] and \
                        player.you.current_position[1] in zones[i].coords[1]:
            player.you.current_zone = zones[i]

    if old_zone != player.you.current_zone:
        print(player.you.current_zone.intro)


def make_known(place):
    # checks if the current Scene.nick is known to the player
    # if not, add it to the player's scenes known
    # should never be called by verb_goto() for any reason
    if place.nick not in player.you.known_locations:
        player.you.known_locations.append(place.nick)
        print("You learned the location of %s!" % place.name)
        print("(You can come back here quickly by typing 'goto %s')" % place.nick)
    pass


def get_current_scene(raise_error=1, s=scenes):
    # finds the current scene based on your coordinates
    for i in range(0, len(s)):
        if tuple(player.you.current_position) == s[i].coords:
            return s[i]

    # sometimes we want to just return nothing
    if raise_error == 1:
        print("Something's wrong, we couldn't get your scene's coordinates.")
        exit(1)


def make_scenes_by_coords(s=scenes):
    # makes a dictionary with keys as Scene coords and values as Scenes
    result = {}

    for i in range(0, len(s)):
        result[s[i].coords] = s[i]

    return result


def incomplete_phrase(input_list):
    # this function is stuck in verb functions that ALWAYS require a
    # a subject and an object in their phrasing

    # the following code must be included in any verb that requires
    # more than one word:

    """
    if len(input_list) == 1:
        incomplete_phrase(input_list)
        return
    """

    print("%s what?" % input_list[0])

###############################################################################
# LIST OF MENUS                                                               #
###############################################################################

advil_elem = MenuElement(items.advil, 0)

convenience_store = Menu([advil_elem])

###############################################################################
# LIST OF SCENES                                                              #
###############################################################################

hospital = Scene("Saint Marie's Hospital",
                 "hospital",
                 (2, 5),
                 "You enter Saint Marie's Hospital. It's whiter than a country club in here.",
                 {'heal': verb_heal, 'cure': verb_cure})

fast_mart = Shop("Fast Mart",
                  "fastmart",
                  (-2, -1),
                  'You enter Fast Mart. You hear a man with a thick Indian accent yell'
                  '"Hello and welcome to Fast Mart!',
                  {},
                  convenience_store)

# shops

# guns_n_bullets
# gregs_crackhouse
# lick_her_store
# vape_nation

# other

# hood_church
# strip_club

###############################################################################
# LIST OF BROTHELS                                                            #
###############################################################################

joes = Brothel("Joe's Fuck Shack",
               "joes",
               (0, 0),
               "You enter Joe's Fuck Shack. It smells of puss.",
               {},
               [actors.cassie, actors.jane, actors.loranda])

trans = Brothel("Tran's Sensual Massages",
                "massage",
                (-3, 1),
                "You enter Tran's Sensual Massage. Exotic plants decorate the lobby.",
                {},
                [actors.mali, actors.fah, actors.ning])

# todo add these
# luxe_city (expensive high-skill hookers)
# shangs_opium_den
# taste_of_africa
# casa_de_cono

###############################################################################
# LIST OF ZONES                                                               #
###############################################################################

hood = Zone("The Hood",
            "hood",
            (range(-10, 11), range(-10, 11)),
            "You are now entering The Hood.")

test = Zone("Test Zone",
            "test",
            (range(11, 21), range(-10, 11)),
            "TEST ZONE PLEASE IGNORE")
