"""
This contains all of the NPCs in the game. All NPCs are Actors, but
many NPCs are instances of subclasses, such as Whore. Any NPC can be
fought or fucked, though they may need to be raped to be fucked.
"""

from random import randint, choice
from copy import deepcopy
import player
import attributes
import combat
import fucking
import loottables


class Hole:
    # holes are parts Actors that the player can fuck. They are
    # contained in the self.holes in a list.
    def __init__(self, name, pleasure):
        self.name = name
        self.pleasure = pleasure


class Actor:
    # ALWAYS INITIALIZE STATS BEFORE ACTORS
    # the stat_block is always an attributes.stat_block class, basically a way of holding
    # statblocks without having to deal with passing 20 variables in order
    # in the same class
    # loot_list is a list of loot tables iterated through to give loot
    # originals is used by random actors for resetting all of the actor's stat_block
    def __init__(self, name, is_random, stat_block, dis_rate, holes, loot_list):
        self.name = name
        self.is_random = is_random
        self.stat_block = stat_block
        self.dis_rate = dis_rate
        self.disease = self.init_diseases(dis_rate)
        self.holes = holes
        self.loot_list = loot_list
        self.originals = []

        self.originals.append(deepcopy(self.stat_block))
        self.originals.append(deepcopy(self.disease))
        self.originals.append(deepcopy(self.holes))
        self.originals.append(deepcopy(self.loot_list))

    def init_diseases(self, dis_val):
        # returns the list of diseases if dis_val is a list, otherwise,
        # it gives random diseases
        if type(dis_val) == list:
            return dis_val
        elif type(dis_val) == int:
            return random_diseases(common_diseases, dis_val)
        else:
            print("ERROR: %s couldn't get diseases." % self.name)

    def fight(self):
        combat.start_combat(self)

    def fuck_ho(self):
        fucking.start_intercourse(player.you, self)

    def infect_player(self, player_dis, condom_on):
        # this function appends player.you.std with any of the Whore's Diseases if
        # the infect chance is low enough
        # check if we even have any diseases
        if len(self.disease) == 0:
            return

        for i in range(0, len(self.disease)):
            # check to see if infectee_2's already has the disease
            # if so, roll the infect roll, and infect if it's low enough
            if check_same_disease(self.disease[i], player_dis) == False and \
                            infect_roll(self.disease[i], condom_on) == True:
                player_dis.append(self.disease[i])
                print("Uh oh! You caught %s!" % self.disease[i].name)  # DBGGR

        return player_dis

    def get_infected_by_player(self, player_dis, condom_on):
        # this basically does the same thing as infect_player but backwards
        # it gives Player disease to the Whore

        if len(player_dis) == 0:
            return

        for i in range(0, len(player_dis)):
            if check_same_disease(player_dis[i], self.disease) == False and \
                            infect_roll(player_dis[i], condom_on) == True:
                self.disease.append(player_dis[i])
                print("%s caught %s" % (self.name, player_dis[i].name))  # DBGGR
        return

    def reset(self):
        # changes the random actor back to normal, to simulate finding
        # a new one when you do

        if self.is_random == 0:
            return

        # it doesn't work unless it's a deepcopy
        self.stat_block = deepcopy(self.originals[0])
        self.disease = deepcopy(self.originals[1])
        self.holes = deepcopy(self.originals[2])
        self.loot_list = deepcopy(self.originals[3])
        # also remove buffs when those are enabled


class Whore(Actor):
    def __init__(self, name, is_random, stat_block, dis_rate, holes, loot_list, age, skill):
        Actor.__init__(self, name, is_random, stat_block, dis_rate, holes, loot_list)
        self.skill = skill
        self.price = self.price_calc()

    def show_specs(self):
        print('Name:\t', self.name)
        print('ADD ANAL, ORAL, VAGINAL')  # DBGGR
        print('Price:\t$%.2f' % self.price)  # http://www.python-course.eu/python3_formatted_output.php

    def price_calc(self):
        subtotal = 60 * self.skill
        if len(self.disease) > 0:
            subtotal /= 2
        # print("%s: %i" % (self.name, subtotal)) # DBGGR
        return subtotal


class Disease:  # these are STDs you can catch
    def __init__(self, name, inf_chance, condom_protects, damage, cure_cost):
        self.name = name
        self.inf_chance = inf_chance
        self.condom_protects = condom_protects
        self.damage = damage
        self.cure_cost = cure_cost


def check_same_disease(infecter, infectee):
    # checks to see if the infecter and infectee already have the same disease
    if len(infectee) == 0:
        return False
    for i in range(0, len(infectee)):
        if infecter == infectee[i]:
            return True
    return False  # if our for loop didn't return true


def infect_roll(disease_rolled, condom_status):
    roll_of_fate = randint(1, 100)
    if disease_rolled.condom_protects is True and condom_status is True:
        return False  # don't infect if condom is on and condom protects
    else:
        pass

    if roll_of_fate <= disease_rolled.inf_chance:
        return True
    else:
        return False


def random_diseases(diseases, val):
    # gives random diseases
    # target is the RandomActor to give the diseases to
    # diseases is the list of all of the diseases that can be caught
    # val is how likely one is to have diseases
    # for every multiple of 100 that val is, the Actor is guaranteed one disease

    target = []
    kl = True

    while val > 100:
        val -= 100
        target = gib_disease(target, diseases)

    while kl == 1:
        # half val and round down til we dont get a disease from rolling
        if randint(0, 100) < val:
            target = gib_disease(target, diseases)
            val /= 2
        else:
            kl = 0

    return target


def gib_disease(tar, dis_list):
    # picks a disease at random and gives it to the Actor if they don't
    # have it already
    l = False
    while l == 0:
        gib = choice(dis_list)
        if gib not in tar:
            tar.append(gib)
            return tar

        # make sure we don't infinite loop from having all the diseases
        if len(tar) >= len(dis_list):
            return tar


###############################################################################
# HOLES                                                                       #
###############################################################################

anus = Hole('anus', 10)
mouth = Hole('mouth', 7)
pussy = Hole('pussy', 15)

male_holes = (anus, mouth)
female_holes = (anus, mouth, pussy)

###############################################################################
# ENEMIES                                                                     #
###############################################################################

# movesets

# Movesets are tuples that contain the names of functions, selected at random
# by the enemy to use on the player.

dindu_moves = (combat.basic_attack, combat.basic_attack) # shoot gun
hobo_moves = (combat.basic_attack, combat.basic_attack, combat.hollar)
watch_seller_moves = (combat.basic_attack, combat.basic_attack)  # watch slap
drug_dealer_moves = (combat.basic_attack, combat.basic_attack)  # throw drugs
policeman_moves = (combat.basic_attack, combat.basic_attack)  # shoot gun, arrest
rabid_dog_moves = (combat.basic_attack, combat.basic_attack)  # infect with rabies
wealthy_guy_moves = (combat.cry, combat.cry)  # run and cower
mugger_moves = (combat.basic_attack, combat.basic_attack) # snatch and grab


# stat_block
dindu_stats = attributes.StatBlock(35, 8, 75, 0, dindu_moves)
hobo_stats = attributes.StatBlock(25, 5, 75, 0, hobo_moves)
watch_seller_stats = attributes.StatBlock(30, 4, 80, 0, watch_seller_moves)
drug_dealer_stats = attributes.StatBlock(40, 10, 65, 25, drug_dealer_moves)
policeman_stats = attributes.StatBlock(100, 12, 95, 45, policeman_moves)
rabid_dog_stats = attributes.StatBlock(25, 6, 75, 0, rabid_dog_moves)
wealthy_guy_stats = attributes.StatBlock(10, 2, 50, -20, wealthy_guy_moves)
mugger_stats = attributes.StatBlock(30, 9, 90, 15, mugger_moves)

# enemies
dindu = Actor(
    'a dindu',  # name
    1,  # is_random
    dindu_stats,  # stats
    [],  # dis_rate
    male_holes,  # holes
    [loottables.dindu_list]
)  # loot_list

hobo = Actor(
    'a hobo',  # name
    1,  # is_random
    hobo_stats,  # stats
    [],  # dis_rate
    male_holes,  # holes
    [loottables.dindu_list]  # loot_list
)

watch_seller = Actor(
    'a watch seller',  # name
    1,  # is_random
    watch_seller_stats,  # stats
    [],  # dis_rate
    male_holes,  # holes
    [loottables.watch_seller_list]  # loot_list
)

drug_dealer = Actor(
    'a drug dealer',
    1,
    drug_dealer_stats,
    [],
    male_holes,
    [loottables.drug_dealer_list]
)

policeman = Actor(
    'a policeman',
    1,
    policeman_stats,
    [],
    male_holes,
    [loottables.policeman_list]
)

wealthy_guy = Actor(
    'a wealthy guy',
    1,
    wealthy_guy_stats,
    [],
    male_holes,
    [loottables.wealthy_guy_list]
)

mugger = Actor(
    'a mugger',
    1,
    mugger_stats,
    [],
    male_holes,
    [loottables.mugger_list]
)

###############################################################################
# DISEASES                                                                    #
###############################################################################

testitis = Disease('testitis', 100, False, 100, None)  # this disease is used for debugging
super_testitis = Disease('super testitis', 100, False, 100, None)  # another disease used for debugging

herpes = Disease('herpes', 4, False, 12, None)
aids = Disease('AIDS', 7, True, 2, None)
chlamydia = Disease('chlamydia', 6, True, 6, None)
gonorrhea = Disease('gonorrhea', 8, True, 4, None)

# common_diseases is used for getting a random disease
common_diseases = [herpes, aids, chlamydia, gonorrhea]

###############################################################################
# WHORES                                                                      #
###############################################################################

# whore moveset

whore_moveset = (combat.basic_attack, combat.cry, combat.whine)

# stats correct order
#
# hp
# atk
# acc
# arm

whore_stats = attributes.StatBlock(15, 3, 65, 0, whore_moveset)

# correct order: 
#
# name
# is_random
# stats
# dis_rate
# holes
# age
# skill

# Joe's Fuck Shack
cassie = Whore('Cassie',  # name
               0,  # is_random
               whore_stats,  # stats
               50,  # dis_rate
               female_holes,  # holes
               [loottables.whore_list],  # loottables
               18,  # age
               3)  # skill

jane = Whore('Jane',  # name
             0,  # is_random
             whore_stats,  # stats
             [super_testitis],  # dis_rate
             female_holes,  # holes
             [loottables.whore_list],  # loottables
             22,  # age
             4)  # skill

loranda = Whore('Loranda',  # name
                0,  # is_random
                whore_stats,  # stats
                75,  # dis_rate
                female_holes,  # holes
                [loottables.whore_list],  # loottables
                25,  # age
                2)  # skill

# todo add these
# Scarlett


# Tran's Sensual Massages
mali = Whore('Mali',  # name
             0,  # is_random
             whore_stats,  # stats
             45,  # dis_rate
             male_holes,  # holes
             [loottables.whore_list],  # loottables
             18,  # age
             6)  # skill

fah = Whore('Fah',  # name
            0,  # is_random
            whore_stats,  # stats
            65,  # dis_rate
            male_holes,  # holes
            [loottables.whore_list],  # loottables
            23,  # age
            5)  # skill

ning = Whore('Ning',  # name
             0,  # is_random
             whore_stats,  # stats
             35,  # dis_rate
             male_holes,  # holes
             [loottables.whore_list],  # loottables
             20,  # age
             4)  # skill


# Streetwalkers
shanice = Whore('Shanice',  # name
                0,  # is_random
                whore_stats,  # stats
                145,  # dis_rate
                female_holes,  # holes
                [loottables.whore_list],  # loottables
                36,  # age
                5)  # skill

krystal = Whore('Krystal',  # name
                0,  # is_random
                whore_stats,  # stats
                225,  # dis_rate
                female_holes,  # holes
                [loottables.whore_list],  # loottables
                25,  # age
                2)  # skill

katya = Whore('Katya',  # name
              0,  # is_random
              whore_stats,  # stats
              115,  # dis_rate
              female_holes,  # holes
              [loottables.whore_list],  # loottables
              15,  # age
              2)  # skill

# todo add these
# Tania
# Imani
streetwalkers = (shanice, krystal, katya)

if __name__ == '__main__':
    print(cassie.disease)
