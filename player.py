"""
This is the player. This is where the player's attributes are kept, such as their
current scene, their cash, and their combat attributes.

todo:
- add functions that show player stats and equipment
- move buffs into their own file

maybe:
- make a subclass for the player's stats in attributes
"""

import attributes
import utils


class Slot:
    # a slot is where a piece of equipment goes, equipment can increase
    # stats, give abilities, you know what the fuck it does
    def __init__(self, name):
        self.name = name
        self.cur_item = None



class Player:
    def __init__(self):
        # The player has a few attributes which aren't self explanatory:
        # in_scene records the player's current scene.
        # std is a list of the player's STDs.
        # nut measures how full the player's balls are. The higher this
        # is, the more often the player can blow his load.
        self.in_scene = None
        self.cash = 10000
        self.std = []
        self.stat_block = your_stats
        self.xp = 0
        self.level = 1
        self.xp_to_level = 100
        self.inventory = []
        # this is used for the player's location in the world
        self.current_position = [0, 0]
        self.current_zone = None
        # these are the player's equipment slots
        self.head = Slot('head')
        self.body = Slot('body')
        self.feet = Slot('feet')
        self.right_hand = Slot('righthand')
        self.left_hand = Slot('lefthand')
        self.all_slots = [self.head, self.body, self.feet, self.right_hand,
                          self.left_hand]
        self.my_buffs = []
        # these are things the player knows
        self.known_spells = []
        self.known_locations = []  # this is a list of nicks of the player's known locations

    ###############################################################################
    # STATS FUNCS                                                                 #
    ###############################################################################

    def add_xp(self, xp_to_add):
        # Adds xp to the player. If the player's XP exceeds
        # self.xp_to_level, they level up.
        self.xp += xp_to_add
        if self.xp >= self.xp_to_level:
            self.xp = 0
            self.level_up()

    def level_up(self):
        # Makes the player level up.
        self.level += 1
        self.xp_to_level = 100 * self.level
        print("You've leveled up to level %i!" % self.level)
        print("Choose a stat to increase!")
        print("- HP")
        print("- Attack")
        print("- Accuracy")
        print("- Armor")
        print("- Mana")
        self.choose_stat_to_increase()

    def choose_stat_to_increase(self):
        all_stats = {  # hp
                       'hp': self.stat_block.hp.level_up_increase,
                       'hitpoints': self.stat_block.hp.level_up_increase,

                       # atk
                       'atk': self.stat_block.atk.level_up_increase,
                       'attack': self.stat_block.atk.level_up_increase,

                       # acc
                       'acc': self.stat_block.acc.level_up_increase,
                       'accuracy': self.stat_block.acc.level_up_increase,
                       'hit': self.stat_block.acc.level_up_increase,

                       # armor
                       'arm': self.stat_block.arm.level_up_increase,
                       'armor': self.stat_block.arm.level_up_increase,
                       'def': self.stat_block.arm.level_up_increase,
                       'defense': self.stat_block.arm.level_up_increase,

                       # mp
                       'mana': self.stat_block.mp.level_up_increase,
                       'mp': self.stat_block.mp.level_up_increase}

        while 1 == 1:  # loop forever
            chosen_stat = input('>')
            try: # remove this try/except
                all_stats[chosen_stat]()
                # self.stat_block.increase(all_stats[chosen_stat])
                return
            except KeyError:
                print("That's not a stat you can increase.")

    def check_nut(self):
        # Returns true if the player has at least 1 nut, otherwise returns false.
        if self.stat_block.nut.val >= 1:
            return True
        else:
            return False

    def show_stats(self):
        print('HP: %s/%s' % (self.stat_block.hp.val, self.stat_block.hp.max_val))
        print('MP: %s/%s' % (self.stat_block.mp.val, self.stat_block.mp.max_val))
        print('Nut: %s' % self.stat_block.nut.val)
        print('--------------------')
        print('Attack (atk): %s' % self.stat_block.atk.val)
        print('Accuracy (acc): %s' % self.stat_block.acc.val)
        print('Armor (arm): %s' % self.stat_block.arm.val)
        print('--------------------')
        print('Sexual Endurance: %s' % self.stat_block.endurance.val)

    def restore_hp(self, heal):
        # This function exists so that the player doesn't go over his max hp
        # when healing.
        # ALWAYS heal the player by calling
        # player.you.restore_hp(heal_amount)
        if self.stat_block.hp.val + heal > self.stat_block.hp.max_val:
            self.stat_block.hp.val = self.stat_block.hp.max_val
        else:
            self.stat_block.hp += heal

    ###############################################################################
    # ITEMS AND INVENTORY FUNCS                                                   #
    ###############################################################################

    def show_inventory(self):
        # first this makes a list of each item in the inventory without duplicates
        # then it prints each item you have and how many of each item you have
        unique_items = list(set(self.inventory))
        for i in range(0, len(unique_items)):
            print("%s ('%s'): %s" % (unique_items[i].name, unique_items[i].nick, self.inventory.count(unique_items[i])))

    def equip(self, item_to_equip):
        # equip() checks all possible slots for a slot that matches the
        # item's slot to fit, unequip()s the current item, then removes
        # the item_to_equip from your inventory and sets the current
        # item in that slot to match item_to_equip.
        for i in range(0, len(self.all_slots)):
            if self.all_slots[i].name == item_to_equip.slot.name:
                self.unequip(self.all_slots[i])
                try:  # remove this try/except
                    self.inventory.remove(item_to_equip)
                except ValueError:
                    print("WARNING: did not exist in inventory")
                self.all_slots[i].cur_item = item_to_equip
                item_to_equip.bonus_stat.mod(item_to_equip.stat_increase_val, 1)
                print(self.head.cur_item)  # DBGGR
                return
        # else   
        print("That slot doesn't exist! Tell the developer to make it.")

    def unequip(self, slot_to_unequip):
        # return item to inventory
        if slot_to_unequip.cur_item is None:
            return
        slot_to_unequip.cur_item.bonus_stat.mod(slot_to_unequip.cur_item.stat_increase_val, 0)
        slot_to_unequip.cur_item = None

    def payment(self, price):
        # this function checks if you have enough money to buy something
        # and blocks the purchase if you don't

        # make price a float
        if type(price) != float:
            try:
                price = float(price)
            except ValueError:
                print("That's not a number!")
                return False

        # if you somehow try to make a negative payment, this stops you
        if price < 0:
            print("You can't pay a negative value!")
            return False

        if self.cash >= price:
            self.cash -= price
            if price != 0:  # so we don't show cash on payments of 0
                self.show_cash()
            return True
        else:
            print("You don't have enough money for that!")
            return False

    def show_cash(self):
        print("You have $%.2f" % self.cash)

    ###############################################################################
    # POSITION FUNCS                                                              #
    ###############################################################################

    def print_current_position(self, in_scene_print=0):
        if in_scene_print == 0:
            print("You're currently at the intersection of %s and %s." % ((self.street_namer(1)), self.street_namer(0)))
        else:
            print("(At the corner of %s and %s.)" % ((self.street_namer(1)), self.street_namer(0)))

    def street_namer(self, ew_or_ns):
        # 0 if we're doing the first (ew) coordinate, 1 if we're doing the second (ns)
        ew_axis = self.current_position[0]
        ns_axis = self.current_position[1]
        our_axis = ew_axis if ew_or_ns == 0 else ns_axis
        not_our_axis = ns_axis if ew_or_ns == 0 else ew_axis

        # first figure out where our axis is and ordinalize it if it's not 0
        if our_axis == 0:
            street_name = 'Hillary Clinton' if ew_or_ns == 1 else 'Main'
        else:
            street_name = '%s' % utils.ordinalize(abs(our_axis))

        # if the other axis is positive, we're either east or north
        # if the other axis is negative, we're either west or south
        # e.g. we're doing the Y axis and our X axis is positive, thus we're east
        if not_our_axis == 0:
            pass
        elif not_our_axis < 0:
            street_name = 'West ' + street_name if ew_or_ns == 1 else 'South ' + street_name
        elif not_our_axis > 0:
            street_name = 'East ' + street_name if ew_or_ns == 1 else 'North ' + street_name

        return '%s %s' % (street_name, 'Street' if ew_or_ns == 1 else 'Road')

# noinspection PyArgumentList
your_stats = attributes.PlayerStatBlock()  # moveset should always be None

# ALWAYS REFER TO THE PLAYER AS "player.you.______"
you = Player()

if __name__ == '__main__':
    # should be at axis of North Hilary Clinton Road and 1st Street
    you.current_position = [3, -4]
    you.print_current_position()
