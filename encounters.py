"""
This module contains all of the encounters in the game while wandering
the hood. Each Encounter is a class, explained in the comments section
under class Encounter.
"""

import input_parser
import player
import actors
from random import randint
import utils

# list of all encounters we have
# redo this so that this autofills in Encounters.__init__()

# THIS IS GLOBAL, SHOULD ONLY BE MODIFIED BY ENCOUNTER.__INIT__()
encounter_list = []


class Encounter:
    # encounters are random events that happen while you're in the hood
    def __init__(self, name, rarity, special, zones):
        self.name = name
        self.rarity = rarity  # lower number is rarest, go no lower than 1
        self.special = special  # here we write the function name we want, no parens
        self.zones = zones
        encounter_list.append(self)

    def add_rarity(self, deck):
        # add ourselves to encounter deck for each number of rarity
        # e.g. rarity is 5, add self to deck 5 times
        # deck should ALWAYS be encounter_deck
        i = 0
        while i <= self.rarity:
            i += 1
            deck.append(self)

###############################################################################
# ENCOUNTER FUNCTIONS                                                         #
###############################################################################

###############################################################################
# HOBO FUNCTIONS                                                              #
###############################################################################


def do_nothing():
    # this does nothing at all
    print("Nothing interesting happens...")
    pass


def hobo_encounter_chooser():
    # this chooses WHICH hobo encounter to use
    # I have no idea why I didn't just make this two different encounters
    hobo_list = {1: hobo_encounter_1, 2: hobo_encounter_2}
    r = randint(1, len(hobo_list))
    hobo_list[r]()


# there are multiple hobo functions, chosen by a random number in a dictionary
# in the above function
def hobo_encounter_1():
    # this one is just a hobo begging for money, if you refuse, he may fight you.
    verb_list = {'fight': actors.hobo.fight,
                 'give': hobo_give_money,
                 'flee': hobo_flee}
    print('A hobo begs you for money! What do you do? You can:\n"combat"\n"flee"\n"give (money)"')
    kl = True
    while kl is True:
        kl = input_parser.special_parser(verb_list)


def hobo_encounter_2():
    # this hobo is drug fueled and always attacks you
    print("Pretend you're fighting a drug_fueled hobo.")
    pass


def hobo_give_money():
    # this asks for how much money you want to give, if you give nothing,
    # you instead get a funny message
    
    # records how much money we have before payment
    before_money = player.you.cash
    
    print("How much money do you give the hobo?")
    paid = False
    while paid is False:
        paid = player.you.payment(input('$'))
    
    # records how much money we have after payment
    after_money = player.you.cash
    
    # if we gave no money, this happens
    if before_money - after_money >= 0:
        print("You tell the hobo to go fuck himself.")
    else:
        print("You give the hobo some spare change. He thanks you.")
        # possibly increase luck if we decide to do have a luck stat
    

def hobo_flee():
    print("You run from the hobo!")

###############################################################################
# HOOKER FUNCTIONS                                                            #
###############################################################################


def whore_encounter_chooser():
    # met_whore gives us a random hooker in the hood
    met_whore = actors.streetwalkers[randint(0, len(actors.streetwalkers) - 1)]
    
    print("You meet a street walker named %s" % met_whore.name)
    met_whore.show_specs()
    
    if player.you.check_nut() is True:
        print("Do you want to fuck %s?" % met_whore.name)
        if utils.yes_or_no() == True and player.you.payment(met_whore.price) == True:
            met_whore.fuck_ho()
    else:
        print("Too bad you don't have enough Nut to do her!")

###############################################################################
# ENCOUNTERS                                                                  #
###############################################################################

none_enc = Encounter('NONE', 6, do_nothing, None)
dindu_enc = Encounter('dindu', 5, actors.dindu.fight, None)
hobo_enc = Encounter('hobo', 3, hobo_encounter_chooser, None)
whore_enc = Encounter('streetwalker', 2, whore_encounter_chooser, None)
# drug dealer encounter
# police encounter
# rabid dog encounter
# wealthy looking guy you can mug encounter (very rare)
# mugger encounter
# watch_hawker encounter


def odds_analysis(encounters):
    # this is only used if we run as main
    total_rarity = 0
    
    # first we get the total rarity (number of cards in the deck)
    for i in range(0, len(encounters)):
        total_rarity += encounters[i].rarity
    # next we divide the rarity by the total rarity and print that as a percentage
    for n in range(0, len(encounters)):
        odds = 100 * (encounters[n].rarity / total_rarity)
        print("Odds of seeing %s: %.2f percent" % (encounters[n].name, odds))


def choose_encounter():
    # this fills our encounter deck with 'cards', then picks one to use

    # this is used to make sure we get encounters from the background scene we want
    # add this later

    # this gets filled with each encounter once per rarity
    encounter_deck = []
    for i in range(0, len(encounter_list)):
        encounter_list[i].add_rarity(encounter_deck)

    # next, generate a random number between 0 and the deck length,
    # then pick that number and pick that encounter.
    r = randint(0, len(encounter_deck) - 1)
    encounter_deck[r].special()

if __name__ == '__main__':
    # if we run this script as main, we instead get an analysis of the odds of
    # all of our encounters
    odds_analysis(encounter_list)
