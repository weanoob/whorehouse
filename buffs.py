"""
This file contains everything related to buffs, including all of the buff-specific functions
There should be a lot of those
"""

import player


class Buff:
    # a buff is a thing like in world of warcraft you know
    # stat_mods should be a list of tuples like so: [(stat1, number1), (stat2, number2)]
    # mod_funcs should be a tuple of functions
    # ALL MOD FUNCTIONS NEED TO TAKE A BOOLEAN ARG CALLED applied,
    # MUST BE True IF THE BUFF IS BEING APPLIED AND False IF THE BUFF IS BEING REMOVED

    def __init__(self, name, nick, stat_mods, mod_funcs, max_stacks=1):
        self.name = name
        self.nick = nick
        self.stat_mods = stat_mods
        self.mod_funcs = mod_funcs
        self.max_stacks = max_stacks

    def apply_buff(self):
        # applies the buff, increasing/decreasing stats

        # first, check to make sure we aren't already at max stacks
        if player.you.my_buffs.count(self) >= self.max_stacks:
            print("Can't apply %s, already at max stacks!" % self.name)  # DBGGR

        # now we increase each stat in stat_mods
        for i in range(0, len(self.stat_mods)):
            self.stat_mods[i][0].mod(self.stat_mods[i][1], 1)
            # player.you.stat_block.mod_stat(self.stat_mods[i][0], self.stat_mods[i][1], 1)

        for j in range(0, len(self.mod_funcs)):
            self.mod_funcs[j](1)

    def remove_buff(self):
        # removes the buff, increasing/decreasing stats

        # first, check to make sure the buff is already on the player
        # if not, return nothing
        if player.you.my_buffs.count(self) >= 0:
            print("Can't remove %s, already at %s stacks!" % (self.name, player.you.my_buffs.count(self)))  # DBGGR

        # now we decrease each stat in stat_mods
        for i in range(0, len(self.stat_mods)):
            self.stat_mods[i][0].mod(self.stat_mods[i][1], 0)

        for j in range(0, len(self.mod_funcs)):
            self.mod_funcs[j](0)

# test_buff


def show_hp(applied):
    if applied == 1:
        print("Applying test buff")
    elif applied == 0:
        print("Removing test buff")
    else:
        print("BUG: applied = ", applied)
    print("HP: ", player.you.stat_block.hp.max_val)


def do_nothing(applied):
    pass

###############################################################################
# LIST OF BUFFS                                                               #
###############################################################################

test_buff = Buff('Test', 'test', [(player.you.stat_block.hp, 10), (player.you.stat_block.atk, 5)], (show_hp, do_nothing))
# todo make these
# weed_buff
# coke_buff
# alcohol_buff

# curse_debuff
# blind_debuff

if __name__ == '__main__':
    # test to make sure buffs are working
    test_buff.apply_buff()
    test_buff.remove_buff()
