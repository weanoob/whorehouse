"""
This is the input parser, quite possibly the most important part of this game
Here, the player inputs stuff, then the parser figure out what to do with it.
First, the verb list (initial_list) is updated with the current scene's commands.
The player inputs two words, their verb and their object (grammatical),
e.g. 'goto hospital' It splits the input by word, finds the verb in initial_list,
calls the function of the verb's value.
"""

import player


def parse_input(start_list):
    # !! RETURN FALSE IF INPUT WAS VALID, TRUE IF IT WAS NOT !!
    # this needs to first separate the string with space as its delimiter
    # stuff should start with a verb
    # after this we have a function for each verb
    # each verb then looks at the following word and player.you.in_scene to determine
    # what to do next
    #
    # initial_list MUST be updated with each verb function name
    # its key is the string input by the player
    # its value is the function 
    # this is done so that we can kind of 'cheat' the normal input routes
    # and basically call functions directly based on player input
    # we basically make a makeshift variable that is a function and saves us a
    # hell of a lot of if-elif-else statements
    #
    # help_on should only be True when called from this file, in other files
    # like encounters.py, it should be False.

    initial_list = dict(start_list)  # so we don't update verb_list

    player_scene = player.you.in_scene  # to make the classes play well

    # Add the current scene's commands to the list
    initial_list.update(player_scene.commands)

    stuff = input('>')
    stuff = stuff.casefold()

    # this if statement will help the player by giving them the list of verbs
    # note: this is SEPARATE from the help function
    if stuff == 'help':
        print("Try typing one of these and a noun:")
        for key in initial_list:
            print('"%s"' % key)
        return True
    elif stuff == 'quit' or 'exit':
        print("Thank you for playing!")
        exit()

    stuff_split = stuff.split()

    # this is to prevent dumbasses from breaking the game by only typing in one word
    if len(stuff_split) <= 1:
        print("%s what?" % (stuff_split[0]))
        return True

    try:
        initial_list[stuff_split[0]](stuff_split)
        return False
        # this should activate the function whose value
        # corresponds to the key which matches the player's
        # input, with the rest of the input passed in
        # E.G. the player inputs "goto", the game then
        # activates the verb_goto() function
    except KeyError:
        print("That doesn't work!")
        return True


# stuff_split[0] doesn't matter, that was used for parse_input
# we pass the whole thing in JUST IN CASE we want to use more than two word
# commands in the future

def special_parser(initial_list, **kwargs):
    # similar to above, but used for special situations like hobo encounters
    # it does not have the one-word limiter

    # kwargs may be necessary in our try if we can get them to be arguments. and
    # if we need arguments in our try

    stuff = input('>')
    stuff = stuff.casefold()
    stuff_split = stuff.split()

    try:
        initial_list[stuff_split[0]]()
    except KeyError:
        print("You can't do that right now!")
        return True
