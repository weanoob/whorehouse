import math
import brothels


class Current:  # this is our makeshift global variable, manipulate it with #Current.in_scene
    in_scene = None  # this should only be changed by enter_scene()
    cash = 10000  # this should only be changed by payment()


class Scene:
    # Scenes are like rooms, you can only be in one scene at a time.
    # The global variable Current.in_scene sets the room you're in.
    #
    # Coords are used by verb_goto() to calculate the distance from your current
    # position to your destination, adding random encounters as appropriate.
    # They're always a two-length tuple, representing X and Y.
    #
    # Commands are dictionaries that are always updated into parse_input()'s
    # verb_list. This is done so that only certain functions can be done in
    # certain places.
    def __init__(self, name, nick, coords, intro, commands):
        self.name = name
        self.nick = nick  # nickname used by the game
        self.coords = coords  # where the scene is on the map
        self.intro = intro  # this is what prints upon entering the scene
        self.commands = commands  # commands you can use in the scene, such as fuck

    def enter_scene(self):
        Current.in_scene = self
        print(self.intro)


def parse_input(stuff):
    # this needs to first separate the string with space as its delimiter
    # stuff should start with a verb
    # after this we have a function for each verb
    # each verb then looks at the following word and Current.in_scene to determine
    # what to do next
    #
    # verb_list MUST be updated with each verb function name
    # its key is the string inputted by the player
    # its value is the function 
    verb_list = {'goto': verb_goto}
    verb_list.update(Current.in_scene.commands)

    stuff = stuff.casefold()
    stuff_split = stuff.split()
    try:
        verb_list[stuff_split[0]](stuff_split)
        # this should activate the function whose value
        # corresponds to the key which matches the player's
        # input, with the rest of the input passed in
        # E.G. the player inputs "goto", the game then
        # activates the verb_goto() function
    except KeyError:
        print("That doesn't work!")


# stuff_split[0] doesn't matter, that was used for parse_input

def verb_goto(input_list):
    # sends you to a scene if it's possible
    # usually, you'll need to go through X random encounters in the hood

    scenes = {joes.nick: joes, hospital.nick: hospital}
    target = scenes[input_list[1]]
    try:  # need to figure out how to do this without try except
        target = scenes[input_list[1]]  # target is where we want to go
    except KeyError:
        print("That place doesn't exist!")
        return

    if Current.in_scene == target:
        print("You're already there!")
    else:
        distance = calc_distance(Current.in_scene.coords, target.coords)
        print(distance)
        while distance > 1:
            distance -= 1
            hood_encounter()
        target.enter_scene()


def show(input_list):
    # this function is going to basically leapfrog us into Whore.show_specs()
    # by letting us use temporary variables that we don't have to make a mess
    # of in our Scene.commands dictionaires, this is probably really bad 
    # coding, but I'm a total nub, so IDGAF
    # it can also leapfrog into other things, such as show_all_hoes()

    # show and tell, get it?
    tell = {'menu': brothels.show_all_hoes, 'hoes': brothels.show_all_hoes}

    # special words are for when we want abnormal behaviors with verbs, then we go right to the function we want
    special_words = ['menu', 'hoes']
    if input_list[1] in special_words:
        # if we our second word is special, then we just activate that function
        tell[input_list[1]]()
    else:
        # we're doing it this way because we actually need to figure out
        # which ho we want, and the best way to do that is to have the
        # brothels module tell us
        wanted_ho = brothels.find_ho(input_list[1], brothels.ho_menu)
        try:
            wanted_ho.show_specs()  # show the specs on the whore
        except AttributeError:
            print("That ho doesn't exist!")


###############################################################################
# NON-VERB-RELATED FUNCTIONS                                                  #
###############################################################################

def payment(price):
    # this function checks if you have enough money to buy something
    # and blocks the purchase if you don't
    if Current.cash >= price:
        Current.cash -= price
        show_cash()
        return True
    else:
        print("You don't have enough money for that!")
        return False


def show_cash():
    print("You have $%.2f" % Current.cash)


def calc_distance(pos1, pos2):
    # calculates the distance between two points
    # args should always be two Scene.coords
    dist = [(pos2[0] - pos1[0]), (pos2[1] - pos1[1])]  # x2 - x1, y2 - y1
    return int(math.sqrt(dist[0] ** 2 + dist[1] ** 2))


def hood_encounter():
    print("ADD RAND")


joes = Scene("Joe's Fuck Shack", "joes", (0, 0), "You enter Joe's Fuck Shack. It smells of puss.", {'show': show})
hospital = Scene("Saint Marie's Hospital", "hospital", (2, 5),
                 "You enter Saint Marie's Hospital. It's whiter than a country club in here.", {})

# sets our starting scene to Joe's Fuck Shack
Current.in_scene = joes

water = 'wet'  # make this loop forever
while water == 'wet':
    parse_input(input('>'))
